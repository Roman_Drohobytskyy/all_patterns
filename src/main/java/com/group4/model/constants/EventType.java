package com.group4.model.constants;

public enum EventType {
    WEDDING,
    BIRTHDAY,
    SAINT_VALENTINES_DAY,
    FUNERAL;
}
