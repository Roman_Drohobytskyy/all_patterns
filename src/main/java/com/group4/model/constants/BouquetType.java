package com.group4.model.constants;

import java.util.Arrays;
import java.util.List;

public enum BouquetType {
    BEAUTY(Arrays.asList(FlowerType.WHITE_CALLA, FlowerType.LILAC, FlowerType.LAVENDER)),
    HAPPYNESS(Arrays.asList(FlowerType.GREEN_AND_BLUE_HYDRANGEA, FlowerType.PION_RUSSIAN_WINE, FlowerType.ROSE_ECUADORIAN)),
    FRIEND(Arrays.asList(FlowerType.DOLPHINUM, FlowerType.SENECIA, FlowerType.EUCALYPTUS)),
    BDAY(Arrays.asList(FlowerType.DOLPHINUM, FlowerType.PION_RUSSIAN_WINE, FlowerType.PION)),
    POHORON(Arrays.asList(FlowerType.SENECIA, FlowerType.ROSE_CREME_PIAGET, FlowerType.LILAC)),
    SMERT(Arrays.asList(FlowerType.ROSE_MENTA, FlowerType.PION_RUSSIAN_WINE, FlowerType.ROSE_ECUADORIAN)),
    REDDEATH(Arrays.asList(FlowerType.ROSE_ROMANTIC_ANTIQUE, FlowerType.ROSE_RED)),
    TASKA(Arrays.asList(FlowerType.SENECIA, FlowerType.ROSE_WHITE, FlowerType.PION)),
    COLORS_OF_FIELD(Arrays.asList(FlowerType.PION_BOWL_OF_CREAM,
            FlowerType.PION_RUSSIAN_WINE, FlowerType.PITTOSPORUM, FlowerType.LILAC, FlowerType.SENECIA)),
    CONTRASTS(Arrays.asList(FlowerType.GREEN_AND_BLUE_HYDRANGEA, FlowerType.LAVENDER, FlowerType.EUCALYPTUS)),
    HONEY(Arrays.asList(FlowerType.ROSE_CREME_PIAGET, FlowerType.ROSE_RED, FlowerType.PION)),
    PROVENCE(Arrays.asList(FlowerType.ROSE_ROMANTIC_ANTIQUE, FlowerType.ROSE_CREME_PIAGET, FlowerType.VANILLA_CLOVES)),
    OMBRE(Arrays.asList(FlowerType.ORCHID, FlowerType.WHITE_CALLA, FlowerType.DUTCH_GREENS, FlowerType.ROSE_ECUADORIAN)),
    LOVE(Arrays.asList(FlowerType.ORCHID, FlowerType.LILAC, FlowerType.WHITE_CALLA, FlowerType.LAVENDER)),
    SEX(Arrays.asList(FlowerType.ROSE_CREME_PIAGET, FlowerType.LILAC, FlowerType.DOLPHINUM, FlowerType.PION)),
    RED_PINK_BOMB(Arrays.asList(FlowerType.PION, FlowerType.POPPY));

    private List<FlowerType> flowers;

    BouquetType(List<FlowerType> flowers) {
        this.flowers = flowers;
    }

    public List<FlowerType> getFlowers() {
        return flowers;
    }
}
