package com.group4.model.constants;

public enum FlowerShopCity {
    LVIV,
    KYIV,
    DNIPRO
}
