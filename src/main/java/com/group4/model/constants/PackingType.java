package com.group4.model.constants;

public enum PackingType {
    GREY_WRAPPER,
    SILVER_WRAPPER,
    BLACK_WRAPPER,
    BLACK_BOX,
    WHITE_WRAPPER,
    WHITE_BOX,
    RED_WRAPPER,
    YELLOW_WRAPPER,
    PINK_WRAPPER,
    VIOLETE_WRAPPER,
    PURPLE_WRAPPER,
    BLUE_WRAPPER;
}
