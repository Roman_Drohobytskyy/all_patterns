package com.group4.model.constants;

public enum FlowerType {
    ROSE_RED,
    ROSE_WHITE,
    ROSE_PINK,
    ROSE_MENTA,
    ROSE_PEONY_PINK,
    ROSE_CREME_PIAGET,
    DOLPHINUM,
    ROSE_ECUADORIAN,
    DUTCH_GREENS,
    POPPY,
    TULIP,
    PION,
    PION_BOWL_OF_CREAM,
    PION_RUSSIAN_WINE,
    PITTOSPORUM,
    LILAC,
    SENECIA,
    GREEN_AND_BLUE_HYDRANGEA,
    ROSE_ROMANTIC_ANTIQUE,
    VANILLA_CLOVES,
    LAVENDER,
    EUCALYPTUS,
    CHAMOMILE,
    MARGUERITE,
    WHITE_CALLA,
    ORCHID
}
