package com.group4.model.shops;

import com.group4.model.Bouqets.Bouquet;
import com.group4.model.constants.BouquetType;
import com.group4.model.constants.EventType;
import com.group4.model.constants.FlowerType;
import com.group4.model.constants.PackingType;

import java.util.List;

public abstract class FlowerShop {

    protected abstract Bouquet createBouquet(BouquetType bouquetType, PackingType packingType);
    protected abstract Bouquet createBouquet(EventType eventType, List<FlowerType> flowerTypes);

    public Bouquet collectBouquet (BouquetType bouquetType, PackingType packingType) {
        Bouquet bouquet = createBouquet(bouquetType, packingType);
        prepareBouqet(bouquet);
        return bouquet;
    }

    public Bouquet collectBouquet(EventType eventType, List<FlowerType> flowerTypes) {
        Bouquet bouquet = createBouquet(eventType, flowerTypes);
        prepareBouqet(bouquet);
        return bouquet;
    }

    private void prepareBouqet(Bouquet bouquet) {
        bouquet.cut();
        bouquet.collect();
        bouquet.pack();
    }
}
