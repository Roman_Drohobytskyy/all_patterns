package com.group4.model.shops;

import com.group4.model.constants.FlowerShopCity;

public class FlowerShopFactory {

        public static FlowerShop createBakery(FlowerShopCity flowerShopCity) {
            if(FlowerShopCity.DNIPRO == flowerShopCity) {
                return new FlowerShopDnipro();
            } else if(FlowerShopCity.LVIV == flowerShopCity) {
                return new FlowerShopLviv();
            } else if (FlowerShopCity.KYIV == flowerShopCity) {
                return new FlowerShopKyiv();
            }
            else return null;
        }
}
