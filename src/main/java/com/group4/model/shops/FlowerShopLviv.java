package com.group4.model.shops;

import com.group4.model.Bouqets.Bouquet;
import com.group4.model.Bouqets.LvivBouquet;
import com.group4.model.constants.BouquetType;
import com.group4.model.constants.EventType;
import com.group4.model.constants.FlowerType;
import com.group4.model.constants.PackingType;

import java.util.List;

public class FlowerShopLviv extends FlowerShop {
    @Override
    protected Bouquet createBouquet(BouquetType bouquetType, PackingType packingType) {
        return new LvivBouquet(bouquetType, packingType);
    }

    @Override
    protected Bouquet createBouquet(EventType eventType, List<FlowerType> flowerTypes) {
        return null;
    }
}
