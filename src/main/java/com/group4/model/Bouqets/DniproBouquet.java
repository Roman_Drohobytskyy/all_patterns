package com.group4.model.Bouqets;

import com.group4.model.constants.BouquetType;
import com.group4.model.constants.PackingType;

public class DniproBouquet extends Bouquet {

    public DniproBouquet(BouquetType bouquetType, PackingType packingType) {
        super(bouquetType, packingType);
    }

    @Override
    public void cut() {
        logger.trace("Dnipro: Cutting flowers for your " + this.getBouquetType() + " in " + this.getPackingType() + '\n');
    }

    @Override
    public void collect() {
        logger.trace("Dnipro: Collecting flowers for your " + this.getBouquetType() + " in " + this.getPackingType() + '\n');

    }

    @Override
    public void pack() {
        logger.trace("Dnipro: Packing your " + this.getBouquetType() + " in " + this.getPackingType() + '\n');
    }
}
