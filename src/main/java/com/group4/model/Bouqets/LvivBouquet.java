package com.group4.model.Bouqets;

import com.group4.model.constants.BouquetType;
import com.group4.model.constants.PackingType;

public class LvivBouquet extends Bouquet{

    public LvivBouquet(BouquetType bouquetType, PackingType packingType) {
        super(bouquetType, packingType);
    }

    @Override
    public void cut() {
        logger.trace("Lviv: Cutting flowers for your " + this.getBouquetType() + " in " + this.getPackingType() + '\n');
    }

    @Override
    public void collect() {
        logger.trace("Lviv: Collecting flowers for your " + this.getBouquetType() + " in " + this.getPackingType() + '\n');

    }

    @Override
    public void pack() {
        logger.trace("Lviv: Packing your " + this.getBouquetType() + " in " + this.getPackingType() + '\n');
    }
}
