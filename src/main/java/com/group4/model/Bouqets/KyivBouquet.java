package com.group4.model.Bouqets;

import com.group4.model.constants.BouquetType;
import com.group4.model.constants.PackingType;

public class KyivBouquet extends Bouquet{

    public KyivBouquet(BouquetType bouquetType, PackingType packingType) {
        super(bouquetType, packingType);
    }

    @Override
    public void cut() {
        logger.trace("Kyiv: Cutting flowers for your " + this.getBouquetType() + " in " + this.getPackingType() + '\n');
    }

    @Override
    public void collect() {
        logger.trace("Kyiv: Collecting flowers for your " + this.getBouquetType() + " in " + this.getPackingType() + '\n');

    }

    @Override
    public void pack() {
        logger.trace("Kyiv: Packing your " + this.getBouquetType() + " in " + this.getPackingType() + '\n');
    }
}
