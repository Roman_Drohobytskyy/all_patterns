package com.group4.model.Bouqets;

import com.group4.model.constants.BouquetType;
import com.group4.model.constants.EventType;
import com.group4.model.constants.FlowerType;
import com.group4.model.constants.PackingType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public abstract class Bouquet {
    private BouquetType bouquetType;
    private PackingType packingType;

    protected static Logger logger = LogManager.getLogger(Bouquet.class);

    public Bouquet(BouquetType bouquetType, PackingType packingType) {
        this.bouquetType = bouquetType;
        this.packingType = packingType;
    }

    public abstract void cut();

    public abstract void collect();

    public abstract void pack();

    public BouquetType getBouquetType() {
        return bouquetType;
    }

    public PackingType getPackingType() {
        return packingType;
    }

    @Override
    public String toString() {
        return "Bouquet{" +
                "bouquetType=" + bouquetType +
                ", packingType=" + packingType +
                '}';
    }
}
