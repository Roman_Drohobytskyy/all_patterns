package com.group4.controller;


import com.group4.model.Bouqets.Bouquet;
import com.group4.model.constants.BouquetType;
import com.group4.model.constants.PackingType;
import com.group4.model.shops.FlowerShop;
import com.group4.model.constants.FlowerShopCity;
import com.group4.model.shops.FlowerShopFactory;
import com.group4.view.View;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Controller {
    private Pattern manualPattern = Pattern.compile("(?i)manual");
    private Pattern exitPattern = Pattern.compile("(?i)exit");
    private Pattern orderBouquetPattern = Pattern.compile("(?i)order\\s+(\\w+)\\s+(\\w+)");
    private Pattern showSomeBouquetPattern = Pattern.compile("(?i)show\\s+bouquet\\s+(\\w+)");
    private Pattern showBouquetsPattern = Pattern.compile("(?i)show\\s+bouquets");
    private Pattern showPackingsPattern = Pattern.compile("(?i)show\\s+packings");

    private String city = "lviv";
    private FlowerShop flowerShop = FlowerShopFactory.createBakery(FlowerShopCity.valueOf(city.toUpperCase()));

    public Controller() {
    }

    private static Logger logger = LogManager.getLogger(Controller.class);

    public Printable execute(View view, String command) throws IllegalArgumentException {
        Printable message = null;
        if (command.matches(manualPattern.pattern())) {
            message = () -> logger.trace(readManual());
        } else if (command.matches(exitPattern.pattern())) {
            message = () -> logger.trace("Bye.");
        } else if (command.matches(showBouquetsPattern.pattern())) {
            message = () -> logger.trace(Arrays.asList(BouquetType.values())+"\n");
        } else if (command.matches(showPackingsPattern.pattern())) {
            message = () -> logger.trace(Arrays.asList(PackingType.values())+"\n");
        } else if (command.matches(orderBouquetPattern.pattern())) {
            Matcher matcher = orderBouquetPattern.matcher(command);
            if (matcher.find()) {
                Bouquet bouquet = flowerShop.collectBouquet(BouquetType.valueOf(matcher.group(1).toUpperCase()),
                        PackingType.valueOf(matcher.group(2).toUpperCase()));
                message = () -> logger.trace(bouquet + "\n");
            }
        } else if (command.matches(showSomeBouquetPattern.pattern())) {
            Matcher matcher = showSomeBouquetPattern.matcher(command);
            if (matcher.find()) {
                message = () -> logger.trace(BouquetType.valueOf(matcher.group(1).toUpperCase()).getFlowers() + "\n");
            }
        } else {
            message = () -> logger.error("Illegal command. Repeat please.\n");
        }

        return message;
    }

    private String readManual() {
        StringBuilder stringBuilder = new StringBuilder();
        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader(
                    "src/main/resources/manual.txt"));
            String line = reader.readLine();
            while (line != null) {
                stringBuilder.append(line);
                stringBuilder.append('\n');
                line = reader.readLine();
            }
            reader.close();
        } catch (IOException e) {
            logger.error(e);
        }
        return stringBuilder.toString();
    }
}
