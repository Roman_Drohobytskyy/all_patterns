package com.group4.view;

import com.group4.controller.Controller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class View {
    private static Logger logger = LogManager.getLogger(View.class);
    private Controller controller;

    public View(final Controller controller) {
        this.controller = controller;
    }

    public void initUI() {
        Scanner scanner = new Scanner(System.in);
        String command;
        logger.trace("(Enter \"manual\" in order to get acquainted)\n");
        do {
            logger.trace("\n>> ");
            command = scanner.nextLine().trim();
            try {
                controller.execute(this, command).print();
            } catch (IllegalArgumentException e) {
                logger.error(e);
            }
        } while (!command.matches("(?i)exit"));
    }
}
